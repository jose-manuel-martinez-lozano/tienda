Rails.application.routes.draw do
  devise_for :admins
  namespace :admin do
  resources :products
end    
    resources :products do
  get 'search', on: :collection
end
    resources :products 
    root :to => 'products#index'
    get 'products/:id/purchase', to: 'products#purchase', as: :purchase_product
    
end